namespace CryptoLine.BL3P
{
	public enum BL3PCurrencyFee : byte
	{
		BTC = 0,
		EUR = 1
	}
}
