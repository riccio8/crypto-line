namespace CryptoLine.BL3P
{
	internal enum BL3POrderStatus
	{
		Pending = 0,
		Open,
		Closed,
		Cancelled,
		Placed
	}
}
