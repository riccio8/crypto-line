using System.Collections.Generic;
using Newtonsoft.Json;

namespace CryptoLine
{
	public sealed partial class ExchangeNDAXAPI
	{
		class WithdrawTemplates : GenericResponse
		{
			[JsonProperty("TemplateTypes")]
			public IEnumerable<string> TemplateTypes { get; set; }
		}
	}
}
